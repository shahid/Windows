$Capslock::Esc
$Esc::Capslock


!q::ExitApp


#b:: ;hide/unhide taskbar (toogle)
if WinExist("ahk_class Shell_TrayWnd")
	{
	WinHide, ahk_class Shell_TrayWnd
	WinHide, ahk_class Shell_SecondaryTrayWnd
	}
Else
	{
	WinShow, ahk_class Shell_TrayWnd
	WinShow, ahk_class Shell_SecondaryTrayWnd
	}
Return

!t::
IF HideTray := !HideTray
{
	WinHide, ahk_class Shell_TrayWnd
	WinHide, ahk_class Shell_SecondaryTrayWnd
}
Else
{
	WinShow, ahk_class Shell_TrayWnd
	WinShow, ahk_class Shell_SecondaryTrayWnd
}
Return

#e:: ;open Home Dir
Run, Explorer  "C:\Users\Mohammed Shahid\"

#del:: ; Empty Recycle Bin
FileRecycleEmpty, C:\
Return

#q::WinClose  A 	; Close Active Window

#v:: 
Run, neovide
return

#+F:: ; Win + F
Run, "C:\Program Files\Everything\Everything.exe" ; Change the path if needed
return

; Map Super+Shift+F to trigger full screen
#f::Send {F11}

;#j::Send !{Tab}
;#k::Send !+{Tab}

#+r:: ;Open Bin Folder
Run, "shell:RecycleBinFolder"
return

; Toggle Touchpad with Windows + T
#t::
Run, % "devcon.exe status ACPI\VEN_06CB\4&37D1C10B&0", , Hide, outputVar
If InStr(outputVar, "running") {
    Run, % "devcon.exe disable ACPI\VEN_06CB\4&37D1C10B&0", , Hide
    MsgBox, Touchpad Disabled
} else {
    Run, % "devcon.exe enable ACPI\VEN_06CB\4&37D1C10B&0", , Hide
    MsgBox, Touchpad Enabled
}
return

#return::
run, powershell
return

 ;Move window to the left side using Win+Shift+L
;#+h::
;WinGet, active_id, ID, A
;WinMove, ahk_id %active_id%, , 0, 0, A_ScreenWidth / 2, A_ScreenHeight
;return


 ;Move window to the right side using Win+Shift+L
;#+l::
;WinGet, active_id, ID, A
;WinMove, ahk_id %active_id%, , A_ScreenWidth / 2, 0, A_ScreenWidth / 2, A_ScreenHeight
;return




